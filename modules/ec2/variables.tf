variable "ami" {
    default = "ami-0d2986f2e8c0f7d01"
}
variable "type" {
    default = "t2.micro"
}
variable "sg_pub_id" {
    type = any
}
variable "sg_pri_id" {
    type =  any
}
variable "vpc" {
    type = any
}
variable "namespace" {
    type = string
}
variable "key_name" {
    type = string
}